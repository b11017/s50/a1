// import { useState, useEffect } from 'react';
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}) {
    console.log(courseProp)
    // console.log(props);
    // result: coursesData[0]- wdc001
    // console.log(typeof props);
    // result: object

    //object destructuring
    const {name, description, price, _id} = courseProp
    // console.log(name);

    //Syntax: const [getter, setter] = useState(initialValueOfGetter)
    // const [count, setCount] = useState(0)
    // const [seats, setSeats] = useState(30)
    // const [isOpen, setIsOpen] = useState(false)

    // const enroll = () => {
    //     if (seats > 0) {
    //         setCount(count + 1);
    //         console.log('Enrollees: ' + count);

    //         setSeats(seats - 1);
    //         console.log('Seats: ' + seats);
    //     } else {
    //         alert("No more seats available");
    //     };
    // }

    // useEffect(() => {
    //     if(seats === 0){
    //         setIsOpen(true)
    //     }
    // }, [seats])

    return (
        <Card className= "mb-4">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Link className="btn btn-primary" to={`/courseView/${_id}`}>View Details</Link>
            </Card.Body>
        </Card>
   
    )
}

