import { Link } from 'react-router-dom'
import {Button, Row, Col} from 'react-bootstrap';

export default function Banner({data}){

	console.log(data)
    const {title, content, destination, label} = data;

	return (
		<Row>
			<Col className = "p-5">
				<h1>{title}</h1>
				<p>{content}</p>
				<Button variant="success" as= {Link} to={destination}>{label}</Button>
			</Col>
		</Row>
	)
}
