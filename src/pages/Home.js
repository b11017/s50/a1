import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {
    const data = {
        title: "Booking-App 182",
        content: "Enroll the courses that we offer",
        destination: "/courses",
        label: "Enroll now!"
    }

    return (
        <>
            <Banner data={data}/>
            <Highlights />
        </>

    )
}

