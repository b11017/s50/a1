import {Form, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom'
export default function Error(){

   return (

      <>
        <h1>Page Not Found</h1>
         <Container>
            <Form>                
               <Link as={Link} to="/homepage"> Go Back to the Homepage</Link> 
            </Form>

         </Container>
      </>




   )
}