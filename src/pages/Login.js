import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'

export default function Login(props) {
    console.log(props);

    const {user, setUser} = useContext(UserContext)
    console.log(user)

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);


    function authenticate(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        fetch('http://localhost:4000/users/login', {
            method: "POST",
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(typeof data.accessToken !== "undefined"){

                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)

                Swal.fire({
                    title: 'Login Successful',
                    icon: 'success',
                    text: 'Welcome to Booking App of 182!'
                })
            } else {

                Swal.fire({
                    title: 'Authentication Failed',
                    icon: 'error',
                    text: 'Check your credentials'
                })
            }
        })

        // localStorage.setItem('email', email)

        // setUser({
        //     email: localStorage.getItem('email')
        // });

        // Clear input fields after submission
        // setter works asynchronously
        setEmail('');
        setPassword('');

        // alert(`${email} has been verified! Welcome back!`);

    }

    const retrieveUserDetails = (token) => {

        fetch('http://localhost:4000/users/getUserDetails', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }


useEffect(() => {

    // Validation to enable submit button when all fields are populated and both passwords match
    if(email !== '' && password !== ''){
        setIsActive(true);
    }else{
        setIsActive(false);
    }

}, [email, password]);

    return (
        
        (user.id !== null) ?
        <Navigate to= "/courses"/>
        :

        <>
        <h1>Login Here:</h1>
        <Form onSubmit={e => authenticate(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

             { isActive ? 
                <Button variant="success" type="submit" id="submitBtn" className="mt-3 mb-5">
                    Submit
                </Button>
                : 
                <Button variant="danger" type="submit" id="submitBtn" className="mt-3 mb-5" disabled>
                    Submit
                </Button>
            }
        </Form>
        </>

    )

}
